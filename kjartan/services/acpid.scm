(define-module (kjartan services acpid)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu services)
  #:use-module (ice-9 match)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages linux)
  #:export (acpid-configuration
            acpid-service
            acpid-service-type))

(define-record-type* <acpid-configuration>
  acpid-configuration make-acpid-configuration
  acpid-configuration?
  (acpid acpid-configuration-acpid
         (default acpid)))

(define acpid-shepherd-service
  (match-lambda
    (($ <acpid-configuration> acpid)
     (list (shepherd-service
            (provision '(acpid))
            (documentation "ACPI Daemon")
            (start #~(make-forkexec-constructor
                      (list (string-append #$acpid "/sbin/acpid") "-f")))
            (stop #~(make-kill-destructor))
            (actions (list (shepherd-configuration-action acpid))))))))

(define acpid-service-type
  (service-type (name 'acpid)
                (description "Test")
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          acpid-shepherd-service)))
                (default-value (acpid-configuration))))

(define* (acpid-service)
  (service acpid-service-type))
