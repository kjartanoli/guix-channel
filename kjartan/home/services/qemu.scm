(define-module (kjartan home services qemu)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-28)
  #:use-module (srfi srfi-171)
  #:use-module (ice-9 match)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix packages)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu home services)
  #:use-module (gnu packages virtualization)
  #:export (qemu-vm-configuration
            qemu-spice-configuration
            qemu-shared-directory
            qemu-socket-network-device
            home-qemu-configuration
            home-qemu-service-type))

(define (positive-integer? val)
  (and (exact-integer? val) (not (negative? val))))

(define (port-number? val)
  (and (exact-integer? val) (not (negative? val))))

(define (port-forwarding-rule? val)
  (and (pair? val) (port-number? (car val)) (port-number? (cdr val))))

(define-maybe string)

(define (list-of-port-forwarding-rules? val)
  (every port-forwarding-rule? val))

(define (security-model? val)
  (memq val '(passthrough mapped-xattr mapped-file none)))

(define (vga-type? val)
  (memq val '(cirrus std vmware qxl tcx cg3 virtio none)))

(define-configuration/no-serialization qemu-shared-directory
  (path string "The path to the directory")
  (tag string "The mount tag of the directory")
  (security-model security-model "The security model of the directory, one of passthrough, mapped-xattr, mapped-file, none"))

(define (list-of-shared-directories? val)
  (every qemu-shared-directory? val))

(define (shared-directory-arguments directory)
  `("-virtfs"
    ,(format "local,path=~a,mount_tag=~a,security_model=~a"
             (qemu-shared-directory-path directory)
             (qemu-shared-directory-tag directory)
             (qemu-shared-directory-security-model directory))))

(define-configuration/no-serialization qemu-spice-configuration
  (port port-number "The port for the SPICE connection")
  (address maybe-string "The IP address spice is listening on")
  (ticketing? (boolean #t) "Require client connects without authentication")
  (copy-paste? (boolean #t) "Enable copy paste between the client and the guest"))

(define (graphics-configuration? val)
  (match val
    ('none #t)
    ('nographic #t)
    ((? qemu-spice-configuration? val) #t)
    (_ #f)))

(define (graphics-arguments config)
  (match config
    ('none '("-display" "none"))
    ('nographic '("-nographic"))
    ((? qemu-spice-configuration? config)
     (match-record config <qemu-spice-configuration>
                   (port address ticketing? copy-paste?)
       `("-spice"
         ,(string-join
           `(,(format "port=~a" port)
             ,@(if (maybe-value-set? address)
                   (list (format "addr=~a" address))
                   '())
             ,(format "disable-ticketing=~a" (if ticketing? "off" "on"))
             ,(format "disable-copy-paste=~a" (if copy-paste? "off" "on")))
           ","))))))

(define-configuration/no-serialization qemu-socket-network-device
  (id string "")
  (listen maybe-string "")
  (connect maybe-string "")
  (model string ""))

(define (qemu-network-device? val)
  (qemu-socket-network-device? val))

(define (list-of-network-devices? val)
  (every qemu-network-device? val))

(define (netdev-arguments config)
  (cond
   ((qemu-socket-network-device? config)
    (match-record config <qemu-socket-network-device>
      (id listen connect model)
      `("-device" ,(format "~a,netdev=~a" model id)
        "-netdev" ,(string-join `("socket"
                                  ,(format "id=~a" id)
                                  ,@(if (maybe-value-set? listen)
                                        (list (format "listen=~a" listen))
                                        '())
                                  ,@(if (maybe-value-set? connect)
                                        (list (format "connect=~a" connect))
                                        '()))
                                ","))))))

(define-configuration/no-serialization qemu-vm-configuration
  (name symbol "The name of the VM")
  (documentation string "Documentation for the VM")
  (memory string "The amount of RAM the VM should have.  Accepts suffixes like M or G to
specify Mega- or Gigabytes.")
  (cpus (positive-integer 1) "The number of CPU cores the VM should have.")
  (disk-image string "The disk image the VM should use.")
  (kvm? (boolean #f) "Whether to enable KVM.")
  (graphics graphics-configuration "The graphics configuration for the VM.")
  (vga (vga-type 'virtio) "The VGA card to emulate.")
  (network-devices (list-of-network-devices '()) "The list of networking devices to set up for the VM.")
  (forward-ports (list-of-port-forwarding-rules '()) "The list of ports to forward to the VM.  Each entry is a pair (HOST . GUEST)")
  (shared-directories (list-of-shared-directories '()) "The list of directories to share with the VM.")
  (auto-start? (boolean #f) "Whether the VM should be started automatically.")
  (extra-args (list-of-strings '()) "Other arguments to pass directly to QEMU."))

(define (qemu-vm-shepherd-service-factory qemu)
  (let ((bin (file-append qemu "/bin/qemu-system-x86_64")))
    (lambda (config)
      (match-record config <qemu-vm-configuration>
        (documentation name auto-start? disk-image cpus memory kvm?
                       graphics vga network-devices forward-ports
                       shared-directories extra-args)
        (shepherd-service
         (documentation documentation)
         (provision (list name))
         (auto-start? auto-start?)
         (start
          #~(make-forkexec-constructor
             '#$(append
                 `(,bin
                   "-hda" ,disk-image
                   "-m" ,memory
                   "-smp" ,(number->string cpus))
                 (if kvm?
                     '("--enable-kvm")
                     '())
                 (graphics-arguments graphics)
                 (list "-vga" (symbol->string vga))
                 (list-transduce
                  tflatten rcons
                  (map netdev-arguments
                       network-devices))
                 (if (null? forward-ports)
                     '()
                     (list "-nic"
                           (string-join
                            `("user"
                              ,@(map (lambda (rule)
                                       (format "hostfwd=tcp::~a-:~a" (car rule) (cdr rule)))
                                     forward-ports))
                            ",")))
                 (list-transduce
                  tflatten rcons
                  (map shared-directory-arguments
                       shared-directories))
                 extra-args)))
         (stop #~(make-kill-destructor)))))))

(define (list-of-qemu-vm-configurations? lst)
  (every qemu-vm-configuration? lst))

(define-configuration/no-serialization home-qemu-configuration
  (qemu
   (package qemu)
   "The qemu package to use")
  (virtual-machines
   (list-of-qemu-vm-configurations '())
   "A list of VM configurations."))

(define (qemu-shepherd-service config)
  (map (qemu-vm-shepherd-service-factory (home-qemu-configuration-qemu config))
       (home-qemu-configuration-virtual-machines config)))

(define home-qemu-service-type
  (service-type
   (name 'qemu)
   (extensions
    (list
     (service-extension home-shepherd-service-type
                        qemu-shepherd-service)))
   (description "A Home service for running QEMU VMs.")))
