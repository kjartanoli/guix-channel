(define-module (kjartan home services emacs)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix packages)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu home services)
  #:use-module (kjartan packages emacs)
  #:use-module (kjartan packages emacs-xyz)
  #:export (emacs-configuration
            emacs-service-type
            org-roam-ui-configuration
            org-roam-ui-service-type))

(define-maybe string)

(define-configuration/no-serialization emacs-configuration
  (emacs
   (package emacs)
   "The emacs package to use")
  (init-dir
   (string "~/.emacs.d/")
   "The directory Emacs should search for init files")
  (name
   maybe-string
   "The name of the Emacs server"))

(define (emacs-args config)
  (let ((name (emacs-configuration-name config)))
    (cons* (string-append "--fg-daemon" (if (maybe-value-set? name)
                                            (string-append "=" name)
                                            ""))
           (string-append "--init-dir=" (emacs-configuration-init-dir config))
           '())))

(define (emacs-shepherd-service config)
  (let ((emacs-bin (file-append
                    (emacs-configuration-emacs config) "/bin/emacs"))
        (emacsclient-bin (file-append
                          (emacs-configuration-emacs config) "/bin/emacsclient")))
    (list (shepherd-service
           (documentation "GNU Emacs daemon")
           (provision '(emacs))
           (start #~(make-forkexec-constructor
                     `(#$emacs-bin #$@(emacs-args config))))
           (stop
            #~(make-system-destructor
               (string-append #$emacsclient-bin "--eval '(kill-emacs)'")))))))

(define (emacs-home-profile-service config)
  (list (emacs-configuration-emacs config)))

(define emacs-service-type
  (service-type
   (name 'emacs)
   (extensions
    (list
     (service-extension home-shepherd-service-type
                        emacs-shepherd-service)
     (service-extension home-profile-service-type
                        emacs-home-profile-service)))
   (default-value (emacs-configuration))
   (description
    "GNU Emacs daemon")))


(define-configuration/no-serialization org-roam-ui-configuration
  (org-roam-ui
   (package emacs-org-roam-ui)
   "The Org Roam UI package to use")
  (auto-start?
   (boolean #t)
   "Whether the service should be started automatically"))

(define (org-roam-ui-shepherd-service config)
  (list (shepherd-service
         (documentation "User interface for Org Roam")
         (provision '(org-roam-ui))
         (requirement '(emacs))
         (start #~(make-system-constructor "emacsclient --eval '(org-roam-ui-mode 1)'"))
         (stop #~(make-system-constructor "emacsclient --eval '(org-roam-ui-mode -1)'"))
         (auto-start? (org-roam-ui-configuration-auto-start? config)))))

(define (org-roam-ui-home-profile-service config)
  (list (org-roam-ui-configuration-org-roam-ui config)))

(define org-roam-ui-service-type
  (service-type
   (name 'org-roam-ui)
   (extensions
    (list
     (service-extension home-shepherd-service-type
                        org-roam-ui-shepherd-service)
     (service-extension home-profile-service-type
                        org-roam-ui-home-profile-service)))
   (default-value (org-roam-ui-configuration))
   (description
    "User interface for Org Roam")))
