(define-module (kjartan packages pdf)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (gnu packages xml)
  #:use-module ((gnu packages pdf) #:prefix gnu:))

(define-public podofo
  (package
    (inherit gnu:podofo)
    (version "0.10.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/podofo/podofo/archive/refs/tags/" version ".tar.gz"))
       (sha256
        (base32
         "1v15903m3irc9ijpkcda4qlcmhwl0dd4v282pdzxvsgr8ck27qjb"))))
    (inputs (modify-inputs (package-inputs gnu:podofo)
              (append libxml2)))
    (arguments '())))
