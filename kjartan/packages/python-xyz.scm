(define-module (kjartan packages python-xyz)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages machine-learning))

(define-public python-pot
  (package
    (name "python-pot")
    (version "0.9.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "POT" version))
       (sha256
        (base32 "11kwc7mdwky6iw7w804z0c4pbi0zh1y5zxvyf8p4fwqa76a27kzf"))))
    (build-system pyproject-build-system)
    ;; Tests seem to depend on networking
    (arguments (list #:tests? #f))
    (propagated-inputs (list python-numpy python-scipy))
    (home-page "https://github.com/PythonOT/POT")
    (synopsis "Python Optimal Transport Library")
    (description "Python Optimal Transport Library")
    (license license:expat)))

(define-public python-gensim
  (package
    (name "python-gensim")
    (version "4.3.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "gensim" version))
       (sha256
        (base32 "1aq8nghmxsj3schg22mmkzaq8hxzxj99zvam07kq41nlzzv6mb4r"))))
    (build-system pyproject-build-system)
    (arguments (list #:tests? #f))
    (propagated-inputs (list python-numpy python-scipy python-smart-open))
    (native-inputs (list python-mock
                         python-pot
                         python-pytest
                         python-pytest-cov
                         python-testfixtures
                         python-visdom))
    (home-page "https://radimrehurek.com/gensim/")
    (synopsis "Python framework for fast Vector Space Modelling")
    (description "Python framework for fast Vector Space Modelling")
    (license #f)))
