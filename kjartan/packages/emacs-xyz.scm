(define-module (kjartan packages emacs-xyz)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module (guix licenses)
  #:use-module (kjartan packages emacs)
  #:use-module (kjartan packages tree-sitter)
  #:use-module ((gnu packages tree-sitter) #:prefix gnu:)
  #:use-module ((gnu packages emacs-xyz) #:prefix gnu:)
  #:use-module ((gnu packages emacs) #:prefix gnu:))

(define-public emacs-calibre
  (package
      (name "emacs-calibre")
      (version "1.4.1")
      (source
       (origin
         (method url-fetch)
         (uri (string-append "https://elpa.gnu.org/packages/calibre-" version ".tar"))
         (sha256
          (base32 "1wjz4d2hrhwcd9ljngygacxm28ddgwndp9krz5cxhjz2dkhs1pgb"))))
      (build-system emacs-build-system)
      (propagated-inputs (list gnu:emacs-compat))
      (home-page "https://git.disroot.org/kjartanoli/calibre.el")
      (synopsis "Interact with Calibre libraries from Emacs")
      (description
       "Interact with Calibre libraries from within Emacs.
View the contents of your library, including much of the metadata
associated with each book, and read them, all from within Emacs.")
      (license gpl3+)))

(define-public emacs-typst-mode
  (let ((commit "5ce6669a28e57ea6f1009c85543d28686212cf32")
        (revision "1")
        (hash "0v8a6hj5psg87j90lskz1z8k6yqip7pn893673q475a2lga008yd"))
    (package
      (name "emacs-typst-mode")
      (version (git-version "0.0" revision commit))
      (source
       (origin (method git-fetch)
               (uri (git-reference
                     (url "https://github.com/Ziqi-Yang/typst-mode.el.git")
                     (commit commit)))
               (sha256 (base32 hash))))
       (build-system emacs-build-system)
       (propagated-inputs (list gnu:emacs-polymode))
       (synopsis "An Emacs major mode for typst, a markup-based typesetting system.")
       (description "An Emacs major mode for typst, a markup-based typesetting system.")
       (home-page "https://github.com/Ziqi-Yang/typst-mode.el")
       (license gpl3+))))

(define-public emacs-emacsql
  (let ((commit "e1baaf2f874df7f9259a8ecca978e03d3ddae5b5")
        (revision "3")
        (hash "0dvqs1jg5zqn0i3r67sn1a40h5rm961q9vxvmqxbgvdhkjvip8fn"))
    (package
      (inherit gnu:emacs-emacsql)
      (version (git-version "3.1.1" revision commit))
      (source
       (origin
         (inherit (package-source gnu:emacs-emacsql))
         (uri (git-reference
               (url "https://github.com/magit/emacsql.git")
               (commit commit)))
         (sha256 (base32 hash))))
      (arguments (list #:emacs emacs))
      (propagated-inputs (modify-inputs (package-propagated-inputs gnu:emacs-emacsql)
                           (append gnu:emacs-sqlite3-api))))))

(define-public emacs-org-roam-ui
  (package
    (name "emacs-org-roam-ui")
    (version "20221105.1040")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/org-roam/org-roam-ui.git")
                    (commit "5ac74960231db0bf7783c2ba7a19a60f582e91ab")))
              (sha256 (base32
                       "0yic5rgp4f1rmi979if79kva7wn3rqnky423mqgf7sdw310h8akl"))))
    (build-system emacs-build-system)
    (propagated-inputs (list gnu:emacs-org-roam
                             gnu:emacs-simple-httpd
                             gnu:emacs-websocket))
    (arguments '(#:include '("^[^/]+.el$" "^[^/]+.el.in$"
                             "^dir$"
                             "^[^/]+.info$"
                             "^[^/]+.texi$"
                             "^[^/]+.texinfo$"
                             "^doc/dir$"
                             "^doc/[^/]+.info$"
                             "^doc/[^/]+.texi$"
                             "^doc/[^/]+.texinfo$"
                             "^out$")
                 #:exclude '("^.dir-locals.el$" "^test.el$" "^tests.el$"
                             "^[^/]+-test.el$" "^[^/]+-tests.el$")))
    (home-page "https://github.com/org-roam/org-roam-ui")
    (synopsis "User Interface for Org-roam")
    (description
     "Org-roam-ui provides a web interface for navigating around notes created within
Org-roam.")
    (license gpl3+)))

(define-public emacs-ligature
  (let ((commit "3d1460470736777fd8329e4bb4ac359bf4f1460a")
        (revision "1")
        (hash "1rnx2mp8y1phnvfirmf4a6lza38dg2554r9igyijl9rgqpjax94d"))
    (package
     (name "emacs-ligature")
     (version (git-version "1.0.0" revision commit))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mickeynp/ligature.el.git")
                    (commit commit)))
              (sha256 (base32 hash))))
     (build-system emacs-build-system)
     (synopsis "Display typographical ligatures in Emacs")
     (description "This package maps ordinary graphemes (characters)
 to fancy ligatures, if both your version of Emacs and the font
 supports it.

With this package you can control where Emacs must display ligatures.
 That is useful if you only want a subset of the ligatures in certain
 major modes, for instance, or if you want to ensure that some modes
 have no ligatures at all.")
     (home-page "https://github.com/mickeynp/ligature.el")
     (license gpl3+))))

(define-public emacs-enwc
  (package
   (name "emacs-enwc")
   (version "2.0")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://elpa.gnu.org/packages/enwc-" version
                                ".tar"))
            (sha256
             (base32
              "17w35b06am5n19nlq00ni5w3jvys9i7swyw4glb7081d2jbij2mn"))))
   (build-system emacs-build-system)
   (home-page "https://savannah.nongnu.org/p/enwc")
   (synopsis "The Emacs Network Client")
   (description
    "ENWC is the Emacs Network Client.  It is designed to provide a front-end to
various network managers, such as NetworkManager and Wicd.  Currently, only
NetworkManager and Wicd are supported, although experimental support exists for
Connman.  In order to use this package, add (setq enwc-default-backend
BACKEND-SYMBOL) where BACKEND-SYMBOL is either wicd or nm, to your .emacs file
(or other init file).  Then you can just run `enwc to start everything.
Example: (setq enwc-default-backend nm)")
   (license gpl3+)))

(define-public emacs-org-timeblock
  (let ((revision "1")
        (commit "086ab1ed2f26757c9faefc454b1f1bf1e2390df9"))
    (package
      (name "emacs-org-timeblock")
      (version (git-version "0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
           (url "https://github.com/ichernyshovvv/org-timeblock.git")
           (commit commit)))
         (sha256
          (base32 "0z7q1qp9nyb0jssmb64wckzj8z12jn2is05lmx5svdz45ypm5x27"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       (list gnu:emacs-compat gnu:emacs-org-ql gnu:emacs-persist))
      (home-page "https://github.com/ichernyshovvv/org-timeblock")
      (synopsis
       "Schedule your day visually, using timeblocking technique inside Emacs")
      (description "The builtin orgmode package for viewing tasks or events
for a particular day, org-agenda, does not help you to quickly understand,
where, for example, you have free time in your day or where you have overlapping
tasks. Just a list of tasks is not sufficient. This package is created to
fix this problem and provide some of the functionality that modern calendars
provide.")
      (license gpl3+))))

(define-public emacs-mines
  (package
    (name "emacs-mines")
    (version "1.6")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://elpa.gnu.org/packages/mines-"
                                  version ".tar"))
              (sha256 (base32
                       "1199s1v4my0qpvc5aaxzbqayjn59vilxbqnywvyhvm7hz088aps2"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/calancha/Minesweeper")
    (synopsis "Minesweeper game")
    (description
     "This is an elisp implementation of the classical minesweeper game.  The target
is localize all hidden mines (bombs) in a rectangular board without detonating
them.  You reveal the content of a cell with the command `mines-dig'.  1.  Cells
with a bomb contain the character x'; if you call `mines-dig in these cells then
you lost the game.  2.  Cells without bomb at distance 1 from any cell with a
mine contain a number: the number of bombs at distance 1 from this cell.  If you
reveal the content of this cell, then this number is shown.  3.  Cells without a
bomb at distance > 1 from any bomb contain  .  If you reveal the content of this
cell, then   is shown and all adjacent cells are recursively revealed.  If you
think that an uncovered cell has a mine, you might flag it with
`mines-flag-cell'; if you call this command again in the same cell the cell is
unflagged.  This is useful to visualize your progress in the game.  The game is
completed once all mine-free cells are revealed, that is, when the only
uncovered cells equals the number of hidden mines.")
    (license gpl3+)))

(define-public emacs-svelte-mode
  (package
    (name "emacs-svelte-mode")
     (version "20211016.652")
     (source (origin
               (method git-fetch)
               (uri (git-reference
                     (url "https://github.com/leafOfTree/svelte-mode.git")
                     (commit "6a1d4274af7f4c0f271f77bd96678c3dd1fa68e1")))
               (sha256 (base32
                        "058mxzcrxqzsax21bs50vysr4ia3jcig83xbns0vhziqpj220yl1"))))
     (build-system emacs-build-system)
     (home-page "https://github.com/leafOfTree/svelte-mode")
     (synopsis "Emacs major mode for Svelte")
     (description
      "This major mode includes @code{JavaScript/CSS} and other language modes as
submode in html-mode.  Mainly based on mhtml-mode.")
     (license lgpl3+)))

(define-public emacs-syncthing
  (package
    (name "emacs-syncthing")
    (version "20240101.2334")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/KeyWeeUsr/emacs-syncthing.git")
             (commit "9f44d45a55b460b7eaeb9fb15d17d94e790705e0")))
       (sha256
        (base32 "13s6gnjxf5g1688hs30ha65nmmby3gyyzpb2wb82hckwgm0g8rqp"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/KeyWeeUsr/emacs-syncthing")
    (synopsis "Client for Syncthing")
    (description
     "This package attempts to port the browser client functionality into Emacs.  The
client requires Syncthing (server) obtainable from https://syncthing.net")
    (license gpl3+)))

(define-public emacs-kotlin-ts-mode
  (package
    (name "emacs-kotlin-ts-mode")
    (version "20231222.1345")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/bricka/emacs-kotlin-ts-mode.git")
             (commit "c884b3442806a21cbcf28ac24853f05da8ac6658")))
       (sha256
        (base32 "139a201kd8hg21jrkww86xdjwx4r586cwv9i1ypclb4mswjahgay"))))
    (build-system emacs-build-system)
    (propagated-inputs (list tree-sitter-kotlin))
    (home-page "https://gitlab.com/bricka/emacs-kotlin-ts-mode")
    (synopsis "A mode for editing Kotlin files based on tree-sitter")
    (description
     "This package uses the `treesit functionality added in Emacs 29 to provide a nice
mode for editing Kotlin code.")
    (license gpl3+)))

(define-public emacs-ocaml-ts-mode
  (package
    (name "emacs-ocaml-ts-mode")
    (version "20230820.1946")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/dmitrig/ocaml-ts-mode.git")
             (commit "bb8c86bd49e4e98f41e45fb0ec82e38f90bc3ee4")))
       (sha256
        (base32 "0wnldxq0syz54my8qdgpwmcn19alj6wk0vhbk0rv1cv6scz13yw4"))))
    (build-system emacs-build-system)
    (propagated-inputs (list gnu:tree-sitter-ocaml))
    (home-page "https://github.com/dmitrig/ocaml-ts-mode")
    (synopsis "Major mode for OCaml using tree-sitter")
    (description "WIP")
    (license gpl3+)))

(define-public emacs-prisma-ts-mode
  (let ((commit "a7029980140ae60612ef876efa17ab81bf4b3add")
        (revision "1")
        (hash "0isym89c4432qrdzpbmg85pw97jw6lvbz9sdv1xy08c448dydg79"))
    (package
      (name "emacs-prisma-ts-mode")
      (version (git-version "0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/nverno/prisma-ts-mode")
               (commit commit)))
         (sha256 (base32 hash))))
      (build-system emacs-build-system)
      (propagated-inputs (list tree-sitter-prisma))
      (home-page "https://github.com/nverno/prisma-ts-mode")
      (synopsis "Prisma schema major mode using tree-sitter")
      (description "Prisma schema major mode using tree-sitter

It provides indentation, font-locking, imenu, and navigation support
for Prisma buffers.")
      (license gpl3+))))

(define-public emacs-gtags-mode
  (package
    (name "emacs-gtags-mode")
    (version "1.6")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://elpa.gnu.org/packages/gtags-mode-" version
                           ".tar"))
       (sha256
        (base32 "1r3ih44kzkrx9bmfl8ri2yv90b7g4nhb0vvdnz1ba3f44x15ppjx"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/Ergus/gtags-mode")
    (synopsis "GNU Global integration with xref, project and imenu.")
    (description
     "GNU Global integration with xref, project, completion-at-point (capf) and imenu.
 There are many other packages with their own approach and set of more
complete/complex features, maps and functionalities; like ggtags, gtags.el,
gxref, agtags and some others referenced in:
https://www.gnu.org/software/global/links.html This package let all the work to
the EMACS tools available for such functions and avoids external dependencies.
Unlike the other packages; this module does not create extra special maps,
bindings or menus, but just adds support to the mentioned features to use
gtags/global as a backend when possible.  We do special emphasis on minimalism,
simplicity, efficiency and tramp support.  This package may be extended in the
future with new features and to support other tools, but only if they are
required and included in an EMACS distribution and don't need external
dependencies.")
    (license gpl3+)))

(define-public emacs-tramp-theme
  (package
    (name "emacs-tramp-theme")
    (version "0.2")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://elpa.gnu.org/packages/tramp-theme-"
                           version ".tar"))
       (sha256
        (base32 "0dz8ndnmwc38g1gy30f3jcjqg5nzdi6721x921r4s5a8i1mx2kpm"))))
    (build-system emacs-build-system)
    (home-page "https://elpa.gnu.org/packages/tramp-theme.html")
    (synopsis "Custom theme for remote buffers")
    (description
     "This is a custom theme for remote buffers.  It is not an own custom theme by
itself.  Rather, it is a custom theme to run on top of other custom themes.  It
shall be loaded always as the last custom theme, because it inherits existing
settings.  This custom theme extends `mode-line-buffer-identification by the
name of the remote host.  It also allows to change faces according to the value
of `default-directory of a buffer.  See `tramp-theme-face-remapping-alist for
customization options.")
    (license gpl3+)))
