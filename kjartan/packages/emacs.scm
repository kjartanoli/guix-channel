(define-module (kjartan packages emacs)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix git-download)
  #:use-module (gnu packages)
  #:use-module ((gnu packages emacs) #:prefix gnu:)
  #:use-module (gnu packages image)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages attr))

(define-public emacs-next-minimal
  (let ((commit "a2c439db687774f7b57959c39560993579c5d1bd")
        (revision "59")
        (hash "0dzdfshnicks4l5w8dlilc28bxfrlrpzdkjzci61qd6bs376vz36"))
    (package
      (inherit gnu:emacs-next-minimal)
      (version (git-version "30.0.50" revision commit))
      (source (origin
                (inherit (package-source gnu:emacs-next-minimal))
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.savannah.gnu.org/git/emacs.git/")
                      (commit commit)))
                (file-name (git-file-name "emacs-next-minimal" version))
                (sha256 (base32 hash)))))))

(define* (emacs->emacs-next emacs #:optional name
                            #:key (version (package-version emacs-next-minimal))
                            (source (package-source emacs-next-minimal)))
  (package
    (inherit emacs)
    (name (or name
              (and (string-prefix? "emacs" (package-name emacs))
                   (string-append "emacs-next"
                                  (string-drop (package-name emacs)
                                               (string-length "emacs"))))))
    (version version)
    (source source)))

(define-public emacs-next (emacs->emacs-next gnu:emacs))
(define-public emacs-next-pgtk (emacs->emacs-next gnu:emacs-pgtk))
(define-public emacs-next-pgtk-xwidgets (emacs->emacs-next gnu:emacs-pgtk-xwidgets))
(define-public emacs (deprecated-package "emacs" emacs-next))
(define-public emacs-pgtk (deprecated-package "emacs-pgtk" emacs-next))
