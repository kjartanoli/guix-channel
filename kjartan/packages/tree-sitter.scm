(define-module (kjartan packages tree-sitter)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system tree-sitter)
  #:use-module ((guix licenses) #:prefix license:))

(define-public tree-sitter-kotlin
  (package
    (name "tree-sitter-kotlin")
    (version "0.3.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/fwcd/tree-sitter-kotlin/archive/refs/tags/" version".tar.gz"))
       (sha256 (base32 "1k88aa94nl6fiwcxpl7q2b96jxxvq39ajplplddn7d0pymkz7dvn"))))
    (build-system tree-sitter-build-system)
    (home-page "https://fwcd.github.io/tree-sitter-kotlin")
    (synopsis "Kotlin grammar for Tree-Sitter")
    (description "Kotlin language grammar for Tree-Sitter.")
    (license license:expat)))

(define-public tree-sitter-prisma
  (package
    (name "tree-sitter-prisma")
    (version "1.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/victorhqc/tree-sitter-prisma/archive/refs/tags/v" version ".tar.gz"))
       (sha256 (base32 "1c1l357pvr9h2nk4jc881nlyz6rhinx14dnsl6d4974z5ai24r3l"))))
    (build-system tree-sitter-build-system)
    (home-page "https://github.com/victorhqc/tree-sitter-prisma")
    (synopsis "Prisma tree sitter grammar")
    (description "Prisma tree sitter grammar")
    (license license:expat)))
