(define-module (kjartan packages ebook)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module ((gnu packages ebook) #:prefix gnu:)
  #:use-module ((gnu packages pdf) #:prefix gnu:)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages digest)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages video)
  #:use-module (kjartan packages pdf)
  #:use-module ((kjartan packages python-xyz) #:prefix kjartan:))

(define-public calibre
  (package
    (inherit gnu:calibre)
    (version "7.26.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://download.calibre-ebook.com/"
                           version "/calibre-"
                           version ".tar.xz"))
       (sha256
        (base32 "09vhqnmmifv0xsbxqqrmdwcbw7vs0h22c277czdcissmfv3kd6qa"))))
    (native-inputs (modify-inputs (package-native-inputs gnu:calibre)
                     (replace "qtbase" qtbase)))
    (inputs (modify-inputs (package-inputs gnu:calibre)
              (append ffmpeg)
              (append cmake)
              (append uchardet)
              (append libxkbcommon)
              (append python-xxhash)
              (append python-fonttools)
              (replace "podofo" podofo)
              (replace "python-pyqt" python-pyqt-6)
              (replace "qtbase" qtbase)
              (replace "qtwebengine" qtwebengine)
              (replace "python-pyqtwebengine" python-pyqtwebengine-6)))
    (arguments
     (list
      ;; Calibre is using setuptools by itself, but the setup.py is not
      ;; compatible with the shim wrapper (taken from pip) we are using.
      #:use-setuptools? #f
      ;; Some of the tests require unrardll.  At first glance the
      ;; license does not look like a free software license (it carries
      ;; restrictions on what you may use the library for).  This means
      ;; it is probably not acceptable into upstream Guix so I do not
      ;; feel like putting in the effort to package it.
      #:tests? #f
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'patch-source
            (lambda _
              (substitute* "src/calibre/linux.py"
                ;; We can't use the uninstaller in Guix. Don't build it.
                (("self\\.create_uninstaller()") ""))))
          (add-after 'patch-source-shebangs 'patch-more-shebangs
            (lambda* (#:key inputs native-inputs #:allow-other-keys)
              ;; Patch various inline shebangs.
              (substitute* '("src/calibre/gui2/preferences/tweaks.py"
                             "src/calibre/gui2/dialogs/custom_recipes.py"
                             "setup/install.py"
                             "setup/linux-installer.sh")
                (("#!/usr/bin/env python")
                 (string-append "#!" (search-input-file inputs "/bin/python")))
                (("#!/bin/sh")
                 (string-append "#!"
                                (search-input-file native-inputs "/bin/sh"))))))
          (add-after 'unpack 'dont-load-remote-icons
            (lambda _
              (substitute* "setup/plugins_mirror.py"
                (("href=\"//calibre-ebook.com/favicon.ico\"")
                 "href=\"favicon.ico\""))))
          (add-before 'build 'configure
            (lambda* (#:key inputs #:allow-other-keys)
              (substitute* "setup/build.py"
                (("\\[tool.sip.bindings.pictureflow\\]")
                 "[tool.sip.bindings.pictureflow]
tags = [\"WS_X11\"]")
                (("\\[tool.sip.project\\]")
                 (string-append "[tool.sip.project]
sip-include-dirs = [\""
                   #$(this-package-input "python-pyqt")
                   "/lib/python3.10/site-packages/PyQt6/bindings\"]")))
              (substitute* "src/calibre/ebooks/pdf/pdftohtml.py"
                (("PDFTOHTML = 'pdftohtml'")
                 (string-append "PDFTOHTML = \""
                                (search-input-file inputs "/bin/pdftohtml")
                                "\"")))
              ;; get_exe_path looks in poppler's output for these
              ;; binaries. Make it not do that.
              (substitute* "src/calibre/utils/img.py"
                (("get_exe_path..jpegtran..")
                 (string-append "'"
                                (search-input-file inputs "/bin/jpegtran")
                                "'"))
                (("get_exe_path..cjpeg..")
                 (string-append "'"
                                (search-input-file inputs "/bin/cjpeg")
                                "'"))
                (("get_exe_path..optipng..")
                 (string-append "'"
                                (search-input-file inputs "/bin/optipng")
                                "'"))
                (("get_exe_path..JxrDecApp..")
                 (string-append "'"
                                (search-input-file inputs "/bin/JxrDecApp")
                                "'")))
              ;; Calibre thinks we are installing desktop files into a home
              ;; directory, but here we butcher the script in to installing
              ;; to calibres /share directory.
              (setenv "XDG_DATA_HOME" (string-append #$output "/share"))
              (substitute* "src/calibre/linux.py"
                (("'~/.local/share'") "''"))
              ;; 'python setup.py rapydscript' uses QtWebEngine, which
              ;; needs to create temporary files in $HOME.
              (setenv "HOME" "/tmp")
              ;; XXX: QtWebEngine will fail if no fonts are available.  This
              ;; can likely be removed when fontconfig has been patched to
              ;; include TrueType fonts by default.
              (symlink (string-append #$(this-package-input "font-liberation")
                                      "/share/fonts")
                       "/tmp/.fonts")
              (let ((podofo #$(this-package-input "podofo")))
                (setenv "PODOFO_INC_DIR"
                        (string-append podofo "/include/podofo"))
                (setenv "PODOFO_LIB_DIR" (string-append podofo "/lib")))
              ;; This informs the tests we are a continuous integration
              ;; environment and thus have no networking.
              (setenv "CI" "true")
              ;; The Qt test complains about being unable to load all image
              ;; plugins, and I notice the available plugins list it shows
              ;; lacks 'svg'. Adding qtsvg-5 doesn't fix it, so I'm not sure how
              ;; to fix it.  TODO: Fix test and remove this.
              (setenv "SKIP_QT_BUILD_TEST" "true")))
          (add-after 'install 'install-rapydscript
            (lambda _
              ;; Unset so QtWebengine doesn't dump temporary files here.
              (unsetenv "XDG_DATA_HOME")
              (invoke "python" "setup.py" "rapydscript")))
          (add-after 'install 'install-man-pages
            (lambda _
              (copy-recursively "man-pages"
                                (string-append #$output "/share/man"))))
          ;; The font TTF files are used in some miscellaneous tests, so we
          ;; unbundle them here to avoid patching the tests.
          (add-after 'install 'unbundle-font-liberation
            (lambda _
              (let ((font-dest
                     (string-append #$output "/share/calibre/fonts/liberation"))
                    (font-src
                     (string-append #$(this-package-input "font-liberation")
                                    "/share/fonts/truetype")))
                (delete-file-recursively font-dest)
                (symlink font-src font-dest))))
          ;; Make run-time dependencies available to the binaries.
          (add-after 'wrap 'wrap-program
            (lambda* (#:key inputs #:allow-other-keys)
              (with-directory-excursion (string-append #$output "/bin")
                (for-each
                 (lambda (binary)
                   (wrap-program binary
                     ;; Make QtWebEngineProcess available.
                     `("QTWEBENGINEPROCESS_PATH" =
                       ,(list
                         (search-input-file
                          inputs "/lib/qt6/libexec/QtWebEngineProcess")))))
                 ;; Wrap all the binaries shipping with the package, except
                 ;; for the wrappings created during the 'wrap standard
                 ;; phase.  This extends existing .calibre-real wrappers
                 ;; rather than create ..calibre-real-real-s.  For more
                 ;; information see: https://issues.guix.gnu.org/43249.
                 (find-files "." (lambda (file stat)
                                   (not (wrapped-program? file)))))))))))))
